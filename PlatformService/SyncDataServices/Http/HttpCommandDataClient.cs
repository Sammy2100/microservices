using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PlatformService.Dtos;

namespace PlatformService.SyncDataServices.Http
{
    public class HttpCommandDataClient : ICommandDataClient
    {
        private readonly HttpClient _httpclient;
        private readonly IConfiguration _configuration;

        public HttpCommandDataClient(HttpClient httpclient, IConfiguration configuration)
        {
            _httpclient = httpclient;
            _configuration = configuration;
        }

        public async Task SendPlatformToCommand(PlatformReadDto platform)
        {
            var httpContent = new StringContent(
                JsonConvert.SerializeObject(platform),
                Encoding.UTF8,
                "application/json"
            );

            var url = $"{_configuration["CommandService"]}api/c/Platforms";
            var response = await _httpclient.PostAsync(url, httpContent);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(" ---> Sync POST to Command Service was ok");
            }
            else
            {
                Console.WriteLine(" ---> Sync POST to Command Service was NOT ok");
            }

        }
    }

}