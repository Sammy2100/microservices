using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ResponseService.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class ResponseController : ControllerBase
    {

        // GET: api/response/5
        [HttpGet("{id:int}")]
        public IActionResult GetAResponse(int id)
        {
            var rnd = new Random();
            var randomInteger = rnd.Next(1, 101);

            if (randomInteger >= id)
            {
                Console.WriteLine(" ---> Failure! Return HTTP Status 500");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            Console.WriteLine(" ---> Success! Return HTTP Status 200");
            return Ok();
        }
    }

}