using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RequestService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RequestController : ControllerBase
    {
        private readonly IHttpClientFactory _clientFactory;

        public RequestController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        [HttpGet]
        public async Task<IActionResult> MakeRequest()
        {
            var httpClient = _clientFactory.CreateClient("RetryPolicy");

            var response = await httpClient.GetAsync("https://localhost:5001/api/Response/50");

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(" ---> Response service returned success");
                return Ok();
            }

            Console.WriteLine(" ---> Response service returned failure");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

}