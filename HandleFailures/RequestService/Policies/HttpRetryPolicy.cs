using System.Net.Http;
using Microsoft.Extensions.DependencyInjection;

namespace RequestService.Policies
{
    public static class HttpRetryPolicy
    {
        public static void RegisterHttpRetryPolicies(this IServiceCollection services)
        {
            //services.AddSingleton<ClientPolicy>(new ClientPolicy());
            services
            .AddHttpClient("ImmeduateRetryPolicy")
            .AddPolicyHandler(r => r.Method == HttpMethod.Get ? new ClientPolicy().ImmediateHttpReply : new ClientPolicy().ImmediateHttpReply);

            services
            .AddHttpClient("LinearHttpReply").AddPolicyHandler(new ClientPolicy().LinearHttpReply);

            services
            .AddHttpClient("ExponentialHttpReply").AddPolicyHandler(new ClientPolicy().ExponentialHttpReply);
        }

    }
}