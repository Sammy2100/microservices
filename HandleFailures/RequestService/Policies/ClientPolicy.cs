using System;
using System.Net.Http;
using Polly;
using Polly.Retry;

namespace RequestService.Policies
{
    public class ClientPolicy
    {
        public AsyncRetryPolicy<HttpResponseMessage> ImmediateHttpReply { get; }
        public AsyncRetryPolicy<HttpResponseMessage> LinearHttpReply { get; }
        public AsyncRetryPolicy<HttpResponseMessage> ExponentialHttpReply { get; }

        public ClientPolicy()
        {
            ImmediateHttpReply = Policy.HandleResult<HttpResponseMessage>(
                res => !res.IsSuccessStatusCode)
                .RetryAsync(5);

            LinearHttpReply = Policy.HandleResult<HttpResponseMessage>(
                res => !res.IsSuccessStatusCode)
                .WaitAndRetryAsync(5, retryCount => TimeSpan.FromSeconds(3));

            ExponentialHttpReply = Policy.HandleResult<HttpResponseMessage>(
                res => !res.IsSuccessStatusCode)
                .WaitAndRetryAsync(5, retryCount => TimeSpan.FromSeconds(Math.Pow(2, retryCount)));
        }

    }
}
